# docker-odoo-l10n-argentina

Imagen odoo de docker localizada para Argentina con datos demo

Podes ver una introudicción a docker y nuestras imágenes en [odoo argentina](http://www.odooargentina.com/page/instalar-usando-docker)

## Características
* Incluye Mútiples repositorios:
    * https://github.com/ingadhoc/odoo-addons
    * https://github.com/ingadhoc/odoo-argentina/
    * https://github.com/ingadhoc/aeroo_reports
    * Otros repositorios de la OCA y de ADHOC SA
* Incluye todas las depedencias requeridas para que dichos módulos funcionen
* Conversión a pdf de aeroo
* Una base de datos de odoo v8.0 con datos demo y módulos tipicos instalados para necesidades contables argentina:
    * cheques
    * distintos medios de pago
    * retenciones
    * percepciones
    * liquidación de impuestos
    * libro iva ventas, libro iva compras
    * modulo para Régimen informativo de compras y ventas - RG 3685
    * factura electrónica y factura electrónica de exportación
    * y mucho más

## Veriones disponibles
* 8.0 Version estable
* 8.0.x Version que utiliza repo o imagenes docker en desarrollo

## Requisitos
Unicamente tener instalado docker [docker](www.docker.com). Lo cual podemos hacer con
```
wget -qO- https://get.docker.com/ | sh
```

## Como usar?
### Imagen de docker
Para usarla basicamente debemos descargar la imagen de docker correspondiente y una vez descargada podremos crear los containers que deseemos con distintas opciones.

Para descargar manualmente (no hace falta ya que al correr por primera vez se descarga automáticamente), podemos correr
```
sudo docker pull adhoc/odoo-l10n-argentina:8.0
```
o, para la versión en desarrollo
```
sudo docker pull adhoc/odoo-l10n-argentina:8.0.x
```

### Corriendo los containers
#### Opciones de docker run
Docker nos ofrece innumerables opciones a la hora de correr un container (docker run).

> Tener en cuenta que luego de correr el container puede demorar unos segundos en estar disponible el servicio para acceder con http://localhost:8069

Presentamos a continuación algunas de las más relevantes para esta imagen:
* '--rm': Hace que nuestro container sea borrado al salir del mismo
* '-ti': se corre de manera interactiva, según el caso nos permite ver el log, adentrarnos en la terminal del container etc
* '--name': nos permite asignar un nombre al container, muy util cuando NO utilizamos la opción --rm
* '-d': correr la imagen en forma "detached", sería como un servicio (no compatible con --rm)
* '-p': utilizado para hacer mapeo de puertos


#### Ejemplos prácticos
Dicho lo anterior, presentamos algunas opciones
> Todos los casos que presentamos van a quedar escuchando en [http://localhost:8069](http://localhost:8069)

Crear container 8.0 y que se borrar al salir
```
sudo docker run --rm -ti -p 8069:8069 adhoc/odoo-l10n-argentina:8.0
```

idem para version en desarrollo
```
sudo docker run --rm -ti -p 8069:8069 adhoc/odoo-l10n-argentina:8.0.x
```


Crear container persistente con nombre "odoo-argentina-80"
```
sudo docker run -d -p 8069:8069 --name odoo-argentina-80 adhoc/odoo-l10n-argentina:8.0
```

Apagar
```
sudo docker stop odoo-argentina-80
```

Levantar
```
sudo docker start odoo-argentina-80
```

Reiniciar
```
sudo docker restart odoo-argentina-80
```

## Como crear imagen localmente
Para 8.0, parado en branch 8.0, en el root del repositorio
```
sudo docker build -t odoo-l10n-argentina:8.0 .
```
Para 8.0.x, parado en branch 8.0.x, en el root del repositorio
```
sudo docker build -t odoo-l10n-argentina:8.0.x .
```

Y luego probamos con
```
sudo docker run --rm -ti -p 8069:8069 odoo-l10n-argentina:8.0
```
o para 8.0.x
```
sudo docker run --rm -ti -p 8069:8069 odoo-l10n-argentina:8.0.x
```
